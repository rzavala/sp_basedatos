function datos(mongoose) {

	var Schema = mongoose.Schema;

	var jProyecto = new Schema({
		nombreProyecto : String,
    	estado : String,
    	prefix : String
	});

	// Retornamos Nombre de la Coleccion, Datos que la Conforman
	var Modelo = mongoose.model('proyectos', jProyecto);
	Modelo.tipo = 'proyecto';
	return Modelo;
}

module.exports.getModeloProyectos = datos;
