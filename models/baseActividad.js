function datos(mongoose) {

	var Schema = mongoose.Schema;

	var jActividad = new Schema({
    idtarea : String,
		nombreProyecto : String,
    actividad : String,
    impacto : String,
    fechacompromiso : String,
    fechacierre : String,
    estatus : String,
    responsable : String,
    codigo_responsable : String,
    ent_evi : String
	});

	// Retornamos Nombre de la Coleccion, Datos que la Conforman
	var Modelo = mongoose.model('actividades', jActividad);
	return Modelo;
}

module.exports.getModeloActividades= datos;
