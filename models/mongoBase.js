var mongoose = require('mongoose');
var mongConecta = new mongoose.Mongoose();

var dirMonBD = 'mongodb://localhost/metro';           // Nombre de la Base de datos

function conectar(servidor){
/*
    if(typeof servidor !== "undefined" && servidor!=='' && servidor!==null) {
       dirMonBD = servidor;
    }

    mongoose.connect(dirMonBD, function(error){
        if(error) {
            console.error("Error en la conexion con Mongoose \n" + error);
            global.conectDB = false;
        }
        else
        {
            console.log("Conectado a Mongoose");
            global.conectDB = true;
        }
    });
*/

    var reconnTimer = null;
    if(typeof servidor !== "undefined" && servidor!=='' && servidor!==null) {
       dirMonDB = servidor;
    }

    // Función para reconexión
    function reconectar(){
        if(mongConecta.connection.readyState == 0){
            mongConecta.connect(dirMonBD,{server : {auto_reconnect: true}}, function(err){
                if(err)
                {
                    console.log('No se conecta a MOngoDB');
                    global.conectDB = false;
                }
                else{
                    console.log('MongoOK');
                    global.conectDB = true;
                    clearTimeout(reconnTimer);
                    reconnTimer = null;
                }
            });
        }
    }

    mongConecta.connection.on('disconnected',function(){
        mongConecta.connection.readyState = 0; // force...
        reconnTimer = setTimeout(reconectar, 1000);
     });

    reconectar();

    return mongConecta;
}

module.exports.getServidorMongBD = conectar;
