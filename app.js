/* Configuracion necesaria */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var objFunciones = require('./routes/index.js');
global.config = require('./config.json');

var socketL = require('socket.io-client')('http://localhost:7500');
socketL.on('connection', function(){
  console.log('---  Conectado Con Gateway Root---------');
});
socketL.on('event', function(data){
  console.log('Un evento Enviado');
  //console.log(data);
});
// Evento recibido desde Gateway para actualizar tiempos de arribo
socketL.on('proyectos', function(data){
  //console.log('Data Client' + data.eventoSocket);
  
  objFunciones.setAdminJson(data, function(resp){
    if(resp.status == 200){
      console.log('Se agregó correctamente el JSON');
      socket.emit('agregado');
    }
    else {
      console.log('Hubo un error');
      console.log(resp);
      socket.emit('falla');
    }
  });
});

socketL.on('disconnect', function(){
  console.log('Desconectado ');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var server=app.listen(global.config.puerto, function () {
  var port = server.address().port
  console.log("Escuchando en puerto:> " + port);
});
/* Fin configuracion */

/*******************************************************************
*   Peticiones GET para revisar si existe la base
********************************************************************/
app.get('/revisaBDD', function(req, res){
  objFunciones.revisaBase(function(resp){
    res.send(resp);
  });
});

/*******************************************************************
*   Peticiones GET para pruebas de funciones
********************************************************************/
app.get('/test', function(req, res){
  objFunciones.getTest(function(resp){
    res.send(resp);
  });
});

app.post('/test', function(req, res){
  objFunciones.postTest(req.body, function(resp){
    res.send(resp);
  });
});
/*******************************************************************
*   Peticiones GET para MicroServicio de "ObtenerListados"
********************************************************************/
app.get('/api/proyecto', function(req, res){
  if(req.query.prefix !== undefined)
  {
    var datos = req.query.prefix.split(",");
    objFunciones.getProyecto(datos, function(resp){
      if(resp.status == 200){
        res.send(resp.message);
      }
      else {
        res.status(resp.status).send(resp);
      }
    });
  }
  else{
     var jResp = {
      status : 400,
      error : 2,
      message : 'La clave de búsqueda no corresponde'
    };
    res.status(jResp.status).send(jResp);
  }
});

app.get('/api/tarea', function(req, res){
  if(req.query.idproyecto !== undefined)
  {
    objFunciones.getTarea(req.query.idproyecto, function(resp){
      if(resp.status == 200){
        res.send(resp.message);
      }
      else{
        res.status(resp.status).send(resp);
      }
    });
  }
  else{
     var jResp = {
      status : 400,
      error : 2,
      message : 'La clave de búsqueda no corresponde'
    };
    res.status(jResp.status).send(jResp);
  }
});

app.get('/api/actividad', function(req, res){
  //console.log("Entro a Actividad");
  if(req.query.idtarea !== undefined && req.query.prefix !== undefined)
  {
    var jDatSea = {
      idtarea : req.query.idtarea,
      prefijo : req.query.prefix
    };
    //objFunciones.getActividad(req.query.idtarea, function(resp){
    objFunciones.getActividad(jDatSea, function(resp){
      //console.log('ppppppp . ');
      //console.log(resp);

      if(resp.status == 200){
        res.send(resp.message);
      }
      else{
        res.status(resp.status).send(resp);
      }
    });
  }
  else{
     var jResp = {
      status : 400,
      error : 2,
      message : 'Las claves de búsqueda no corresponde'
    };
    res.status(jResp.status).send(jResp);
  }
});

app.get('/api/visor', function(req, res){
  objFunciones.getVisorProyectos(function(resp, error){
    if(error){
        res.status(500).send(resp);
    }
    res.status(resp.status).send(resp);
  });
});

app.get('/api/actividadesVisor', function(req, res){
  objFunciones.getVisorActividadesV(function(resp){
    res.send(resp);
  });
});

/*******************************************************************
*   Peticiones GET para MicroServicio de "Notificaciones"
*******************************************************************/

app.get('/api/email', function(req, res){
  objFunciones.getUsuarios(function(resp){
    if(resp.status == 200){
      var jData = {
        usuarios : resp.message
      };
      res.send(jData);
    }
    else {
      res.status(resp.status).send(resp);
    }
  });
});

app.get('/api/proyecto/actividad', function(req, res){
  console.log('Petición: ' + req.query.codigousuario);
  objFunciones.getActividadesUser(req.query.codigousuario, function(resp){
    if(resp.status == 200){
      res.send(resp.message);
    }
    else{
      res.send(resp);
    }
  });
});

/*******************************************************************
*   Peticiones POST para "GOOGLE DRIVE"
*******************************************************************/

/********************************************************************************
* Métodos Post para insertar usuarios
@param array de Json :
{
  "usuarios" : [
    {
      "codigo_empleado":"001",
      "email": "xochitl.caballero@cidesi.edu.mx",
      "prefijo" : ["CRUM", "MONFET"]
    },
    ......
  ]
}
********************************************************************************/
app.post('/api/usuarios', function(req, res){
  console.log('Total de usuarios....');
  console.log(JSON.stringify(req.body));
  if(req.body.usuarios !== undefined)
  {
    var tam = req.body.usuarios.length
    if(tam > 0)
    {
      // HAcemos la petición para eliminar primero
      objFunciones.deleteBase(function(resp){
        if(resp.status == 200)
        {
          console.log('Borro todo en MongoDB');
          objFunciones.setUsuario(req.body.usuarios, function(resp){
            if(resp.status == 200){
              console.log('>>>> ' + resp.message);
              res.send(resp.message);
            }
            else{
              console.log('Error al insertar usuarios');
              console.log(resp);
              res.send(resp);
            }
          });
        }
        else{
          console.log('Error al eliminar todo');
          console.log(resp);
          resp.send(resp);
        }
      });
    }
    else {
      var jResp = {
       status : 400,
       error : 1,
       message : 'No se encontro array de usuarios'
     };
     res.status(jResp.status).send(jResp);
    }
  }
  else {
    var jResp = {
     status : 400,
     error : 2,
     message : 'Falta el JSON con datos a insertar'
   };
   res.status(jResp.status).send(jResp);
  }
});

/*******************************************************************
 * Instrucción para borrar todas las colecciones de la Base de Datos
 * @param jSON con todo en uno, proyecto, tareas, actividades
 *******************************************************************/
app.post('/api/adminProyectos', function(req, res){
   console.log('Tenemos El Mega JSON ---- ' + new Date());
   //res.status(200).send('resp');
   console.log(req.body);
  /*
  objFunciones.setAdminJson(req.body, function(resp){
    if(resp.status == 200){
      res.send(resp);
    }
    else {
      res.send(resp.message);
    }
  });
  */
  
});

/*******************************************************************
 * Instrucción para borrar todas las colecciones de la Base de Datos
 *******************************************************************/
app.delete('/', function(req, res){
  console.log('Peticion de borrar');
  objFunciones.deleteBase(function(resp){
    res.send(resp);
  });
});
