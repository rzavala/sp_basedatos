var request=require('request');
var async = require('async');

// Variables que hacen referencia a las Colecciones de Mongo para Plataforma
var mongoose = require('../models/mongoBase.js').getServidorMongBD();
// Modelos de los Schemas de la base de datos
var PackageProyecto = require ('../models/baseProyecto.js').getModeloProyectos(mongoose);
var PackageTarea = require ('../models/baseTarea.js').getModeloTareas(mongoose);
var PackageActividad = require ('../models/baseActividad.js').getModeloActividades(mongoose);
var PackageUsuario = require ('../models/baseUsuarios.js').getModeloUsuarios(mongoose);

// Almacena los manejadores de todas las colecciones
var coleccionesManager = {
    managerProyecto : '',
    managerTarea : '',
    managerActividad : '',
    managerUsuario : ''
  };

global.mongo = mongoose;

module.exports = {

  revisaBase : function(fun){
    revisaBaseDatos(function(respuesta){
      console.log('Revisamos BDD....');
      fun(respuesta);
    });
  },

  getTest : function(fun){
    // infoError('Se lanzó la llamada POST');
    // fun('Respondiendo desde INdex');
    /*
    var sentencia = { $and: [{idproyecto:'GKN'}, {estatus:'Atrasado'}]};

    buscaDato(sentencia, PackageTarea).then(function(resp){
      console.log(resp);
      console.log('-------------');
      estadoProyecto(["GKN", "MONFET"], function(resp){
        console.log(resp);
      });
      fun(resp);
    },function(error){
      console.log(error);
      fun(resp);
    });
    */

  },

  postTest : function(data, fun){
    console.log('Tam Datos ' + data.tareas.length);
    var posi = 0;
    async.forEach(data.tareas, function(item, callback){
      console.log(posi + '.- ' + item.actividad);
      posi++;
      if(posi == data.tareas.length-1)
      {
        callback('UOO');
        fun('OK');
      }

    });
  },

  /****************************************************************************************
   * Sección de funciones GET para los diferentes microservicios que lo necesitan
   ***************************************************************************************/
  getProyecto : function(data, fun){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        var sentenciaBusqueda = {'prefix': {'$in':data}};
        buscaDato(sentenciaBusqueda, PackageProyecto).then(function(resp){
          if(resp.message.length > 0)
          {
            var tamRe = resp.message.length;
            console.log(resp);
            var cuenta = 0;
            async.forEach(resp.message, function(el){
              var sentencia = { $and: [{idproyecto:el.prefix}, {estatus:'Con atraso'}]};
              console.log(el.prefix);
              buscaDato(sentencia, PackageTarea).then(function(respEdo){
                console.log(respEdo);
                if(respEdo.message.length > 0)
                {
                  // Si tiene atrasos, así que estado es 0
                  el.estado = 0;
                  cuenta++;
                }
                else{
                  // dataResp.push(1);
                  el.estado = 1;
                  cuenta++;
                }
                if(cuenta == tamRe)
                  fun(resp);
              });
            });
          }
          else
          {
            console.log('No existe el proyecto');
            fun(resp);
          }
        }, function(error){
          fun(resp);
        });
      }
      else
      {
        fun(resp);
      }
    });
  },

  getTarea : function(data, fun){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        buscaDato({'idproyecto':data}, PackageTarea).then(function(resp){
          fun(resp);
        }, function(error){
          fun(resp);
        });
      }
      else
      {
      fun(resp);
      }
    });
},

  getActividad : function(data, fun){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        //buscaDato({'idtarea':data}, PackageActividad).then(function(resp){
        buscaDato({$and:[{idtarea:data.idtarea}, {nombreProyecto:data.prefijo}]}, PackageActividad).then(function(resp){
          console.log('Total: ' + resp.message.length);
          fun(resp);
        }, function(error){
          fun(resp);
        });
      }
      else
      {
        fun(resp);
      }
    });
  },

  getUsuarios : function(fun){
    buscaDato({}, PackageUsuario).then(function(resp){
      fun(resp);
    }, function(error){
      fun(resp);
    });
  },

  getVisorProyectos : function(fun, error){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        // console.log('getVisorProyectos ...');
        var sentencia = {};
        // Obtenemos Lista de proyectos
        listaProyectos(function(resp){
           // console.log(resp);
          if(resp.status == 200)
          {
             // console.log('Tenemos Los Proyectos');
            ordenaProyectos(resp.message, function(resp){
                fun(resp);
            });
          }
          else {
            // manda error de conexion
            fun(resp, resp.status);
          }
        });
      }
      else
      {
        fun(resp);
      }
    });
  },

   getActividadesUser : function(data, fun){
     statusBDD(function(resp){
      if(resp.status == 200)
      {
        console.log('Valores a buscar - : ' + data);
        buscaDato({'codigoEmpleado':data}, PackageUsuario).then(function(rp){
          if(rp.status == 200 && rp.message.length > 0)
          {
            var email = rp.message[0].email;
            var prefijos = rp.message[0].prefijo;
            console.log('Email ' + email);
            // Buscar Actividades
            buscaAct(data, email, prefijos, function(resp){
              fun(resp);
            });
          }
          else
          {
              var jResp = {
                status : 400,
                error : 1,
                message : { proyectos :[]}
              };
              fun(jResp);
          }
        }, function(err){
          var jResp = {
                status : 400,
                error : 1,
                message : { proyectos :[]}
              };
              fun(jResp);
        });
      }
      else
      {
        var jResp = {
          status : 400,
          error : 1,
          message : 'No Existe conexión con MongoDB'
        };
        fun(jResp);
      }
     });
  },

/*
Retorna arreglo con actividades con retraso
*/
  getVisorActividadesV : function(fun){
    statusBDD(function(resp){
      if(resp.status == 200){
        buscaDato({'estatus':'Con atraso'}, PackageActividad).then(function(rp){
          if(rp.status == 200 && rp.message.length > 0)
          {
            // console.log('saliendo ');
            fun(rp);
          }
          else
          {
              var jResp = {
                status : 200,
                error : 0,
                message : []
              };
              fun(jResp);
          }
        }, function(error){
          var jResp = {
              status : 400,
              error : 1,
              message : []
            };
          fun(jResp);
        });
      }else{
        fun(resp);
      }
    });
  },

  /****************************************************************************************
   * Sección de funciones POST para los diferentes microservicios que lo necesitan
   ***************************************************************************************/
  setUsuario : function(data, fun){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        addUsers(data, function(resp){
          fun(resp)
        });
        /*
        nuevosUsuarios(data, function(resp){
          //console.log('Retornamos');
          fun(resp);
        });
        */
      }
      else
      {
        fun(resp);
      }
    });
  },

  /****************************************************************************************
   * Borra Base de datos
   ***************************************************************************************/
  deleteBase : function(fun){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        var jResp = {
          status : 200,
          error : 0,
          message : 'Todo está limpio'
        };
        PackageProyecto.remove({},function(error){
        if(error)
          console.log('Hubo Error: ' + error);
        });
        PackageTarea.remove({},function(error){
          if(error)
            console.log('Hubo Error: ' + error);
        });
        PackageActividad.remove({},function(error){
          if(error)
            console.log('Hubo Error: ' + error);
        });
        PackageUsuario.remove({},function(error){
          if(error)
            console.log('Hubo Error: ' + error);
        });
        fun(jResp);
      }
      else
      {
        jResp.status = 400,
        jResp.error = 3,
        jResp.message = 'No existe conexión con MongoDB'
        fun(jResp);
      }
    });
  },
  /****************************************************************************************
   * Sección de funcion POST para insertar todos los proyectos
   ***************************************************************************************/
  setAdminJson : function(jMega, fun){
    statusBDD(function(resp){
      if(resp.status == 200)
      {
        console.log('Conexión con BDD ok');
        manageProy(jMega, function(resp){
          if(resp.status == 200)
          {
              fun(resp);
          }
          else {
            fun(resp);
          }
        });
      }
      else
      {
        jResp.status = 408,
        jResp.error = 1,
        jResp.message = 'No se tiene conexión con MongoDB'
      fun(jResp);
    }
    });
  },

};  //Exports

/*************************************************************************************
 * Descripción: Busca las actividades en proceso correspondientes al usuario especificado
 * y en base a los prefijos que tiene asignados
 * Parámetro de entrada:
  @param idUsuario: Numero del responsable
  @param email: Email del responsable
  @param prefijos: Array con los prefijos de los proyectos a los que pertenece
 * Parámetro de salida:
  @return callback con arreglo de JSON con los datos ordenados
 *************************************************************************************/
function buscaAct(idUsuario, email, prefijos, callback){
  statusBDD(function(resp){
    if(resp.status == 200)
    {
      console.log('Buscando actividades de: ' + idUsuario);
      console.log('prefijos + ' + prefijos);
     //PackageActividad.find({$and:[{codigo_responsable:idUsuario}, {estatus:'En proceso'}]}).count(function(err, cuenta){

      //PackageActividad.find({nombreProyecto:{$in:prefijos}, codigo_responsable:idUsuario, estatus:'En proceso'}).count(function(err, cuenta){
//      PackageActividad.find({nombreProyecto:{$in:prefijos}, codigo_responsable:idUsuario, estatus:'Con atraso'}).count(function(err, cuenta){
//      db.actividades.find({$and:[{nombreProyecto:{$in:['QD0458']}, codigo_responsable:'9996'}, {$or:[{estatus:'Cerrado'},{estatus:'Con atraso'}]}]}).count()
      PackageActividad.find({$and:[{nombreProyecto:{$in:prefijos}, codigo_responsable:idUsuario}, {$or:[{estatus:'En proceso'},{estatus:'Con atraso'}, {estatus:'En Proceso'},{estatus:'Con Atraso'}]}]}).count(function(err, cuenta){
      //PackageActividad.find({$and:[{nombreProyecto:{$in:prefijos}}, {codigo_responsable:idUsuario}, {estatus:'En proceso'}, {estatus:'Con atraso'}]}).count(function(err, cuenta){
        if(err)
        {
            var jResp = {
            status : 400,
            error : 1,
            message : 'No tiene actividades el usuario'
            };
            console.log(err);
            callback(jResp);
        }
        else
       {
           if(cuenta > 0)
           {
            console.log('Tenemos actividades: ' + cuenta);
            //PackageActividad.find({$and:[{codigo_responsable:idUsuario}, {estatus:'En proceso'}]}, null, {sort: {nombreProyecto:-1, idtarea:-1}},
            PackageActividad.find({$and:[{nombreProyecto:{$in:prefijos}, codigo_responsable:idUsuario}, {$or:[{estatus:'En proceso'},{estatus:'Con atraso'}]}]}, null, {sort: {nombreProyecto:-1, idtarea:-1}},
            function(err, actividades){
                if(err)
                {
                    var jResp = {
                        status : 400,
                        error : 1,
                        message : 'No tiene actividades el usuario'
                    };
                    console.log(err);
                    callback(jResp);
                }
                else
                {
                    console.log('Si dió Actividades: ' + actividades.length);
                    ordenaActividades(actividades, email, function(ordenados){
                        var jResp = {
                            status : 200,
                            error : 0,
                            message : ordenados
                            };
                        callback(jResp);
                  });
                }
            }); // Busca Actividades
           }
           else
           {
             var jResp = {
                status : 400,
                error : 1,
                message : { proyectos :[]}
                };
             callback(jResp);
            }
       }
     }); // Cuenta Actividades
    }
    else
    {
    var jResp = {
        status : 400,
        error : 1,
        message : 'No Existe conexión con MongoDB'
    };
    callback(jResp);
    }
  });//Checa Base de datos conexión
}

/*************************************************************************************
 * Descripción: Busca el nombre del proyecto en base al prefijo
 * Parámetro de entrada:
  @param prefijo: String con el prefijo del proyecto a buscar
 * Parámetro de salida:
  @return callback con arreglo de JSON con los datos del proyecto
 *************************************************************************************/
function buscaNombProy(prefijo, callback)
{
    statusBDD(function(resp){
        if(resp.status == 200)
        {
            PackageProyecto.find({'prefix':prefijo}, function(err, nom){
                if(err)
                {
                    var jResp = {
                    status : 400,
                    error : 1,
                    message : 'No se obtuvo nombre del proyecto'
                    };
                    callback(jResp);
                }
                else
                {
                   var jResp = {
                        status : 200,
                        error : 0,
                        message : nom[0].nombreProyecto
                        };
                    callback(jResp);
                }
            });
        }
        else
        {
            var jResp = {
                status : 400,
                error : 1,
                message : 'No Existe conexión con MongoDB'
            };
            callback(jResp);
        }
    });
}

/*************************************************************************************
 * Descripción: Revisa que exista conexión con la base de MongoDB
 * Parámetro de entrada:
  @param proyectos: Ninguno
 * Parámetro de salida:
  @return callback con arreglo de JSON con el resultado de la revisión
 *************************************************************************************/
function statusBDD(callback)
{
  //console.log('Checando BDD Conexión');
  if(global.conectDB)
  {
    var jResp = {
      status : 200,
      error : 0,
      message : ''
    };
    callback(jResp);
  }
  else
  {
    var jResp = {
      status : 400,
      error : 3,
      message : 'No existe conexión con MongoDB'
    };
    callback(jResp);
  }
}

/*************************************************************************************
 * Descripción: Ordena los nombres con sus tareas correspondientes en un JSON para el
 * carrusel de proyectos
 * Parámetro de entrada:
  @param proyectos: Array con los proyectos existentes
 * Parámetro de salida:
  @return callback con arreglo de JSON con los datos ordenados
 *************************************************************************************/
function ordenaProyectos(proyectos, callback)
{
  var jsonRespuesta = {
    "status" : 200,
    "error" : 0,
    "message" : '',
    "proyectos": []
  };

  var nombresProyectos = [];

  if(proyectos.length > 0)
  {
    for(var i=0, j=0; i < proyectos.length; i++)
    {
      nombresProyectos[proyectos[i].prefix] = proyectos[i].nombreProyecto;

      //console.log(i + 'Buscando: ' + extraccion + ' _id: ' + nombre + ' name: ' + nombreProy);
      PackageTarea.find({'idproyecto':proyectos[i].prefix}, function(err, resp){
        j++;
        if(resp.length > 0)
        {
          // console.log(i + ' --- ' + j + ' ... ' + proyectos[j-1].nombreProyecto);
          jsonRespuesta.proyectos.push({
            "nombreProyecto": nombresProyectos[resp[0].idproyecto],      // nombreProy,
            "tareas": resp
          });
        }
        if(j == proyectos.length)
        {
          callback(jsonRespuesta);
        }
      }).sort({'_id':1}); //find Proyectos
    }
  }
  else
  {
    callback(jsonRespuesta);
  }
}

/*************************************************************************************
* Funcionalidad POST para enviar email cuando se genera un error en alguna función
*************************************************************************************/
function infoError(error)
{
  var headers = {
    'User-Agent' : 'Super Agent/0.0.1',
    'Content-Type' : 'application/json'
  };

  var postData = {
    fecha : new Date(),
    microservicio : 'MongoDB',
    mensaje : error
  }
  /*
  {
     username : "cidesi",
     password : "4XkgKUYvwZ"
  };
  */

/*
  {
    fecha : new Date(),
    microservicio : 'MongoDB',
    mensaje : error
  }
*/
  console.log('Direccion: ' + global.config.dirError);
  //api/error
  var options = {
    url : global.config.dirError,
    method : 'POST',
    json : true,
    headers : headers,
    body: postData
  };

  request(options, function(error, response, body){
    if(!error && response.statusCode == 200)
    {
      console.log(body);
    }
    else
    {
      console.log(response.body);
      console.log('Hubo un Error');
    }
  });

}

/*************************************************************************************
 * Descripción: Ordena las actividades pendiente del usuario y las agrupa por proyectos
 * Parámetro de entrada:
  @param act: Array con las actividades que tiene asignadas el usuario
  @param email: Email del responsable
 * Parámetro de salida:
  @return callback con arreglo de JSON con los datos ordenados
 *************************************************************************************/
function ordenaActividades(act, email, callback)
{
  var tam = act.length;
  var proyectos = '';

  var jResultado = {
    status : 200,
    error : 0,
    message : 'Saliendo del Ordenamiento'
  };

  var jCAL = {
    status : 200,
    error : 0,
    message : 'Saliendo del Ordenamiento'
  };

  var prefActual = '';

  // JSON FINAL
  var jFinal = {
    proyectos : []
  };
  // JSON con Actividades Acumuladas
  var jActividad = {
    nombreProyecto : '',
    email : email,
    actividades : []
  };
  console.log('   ¿¿¿¿¿    ' + tam);
  // Obtenemos el listado de proyectos
  PackageProyecto.find({}, function(err, resp){
    if(resp.length > 0)
    {
      proyectos = resp;
      console.log('Tenemos Proyectos');
      // console.log(JSON.stringify(proyectos));
      console.log('------- ----------    ----------------   -------');
     // Verificamos el tamaño de las actividades
     if(act.length > 1)
     {
      var posi = 0;
      prefActual = act[0].nombreProyecto;
      var proyecto = '';
      act.forEach(function(el){
        console.log(posi + ' >>>   ' + el.actividad + ' -- ' + el.nombreProyecto);
        shP(proyectos, el.nombreProyecto, function(name){
          console.log(posi +' - Name: ' + name);
          // Revisamos Proyecto 0
          if(posi == 0)
          {
            // Primero vamos a modificar el nombre del proyecto
            jActividad.nombreProyecto = prefActual + ' - ' + name;

            // Armamos JSON de Actividad y lo agregamos
            var jAct = {
              actividad : el.actividad,
              fechacompromiso : el.fechacompromiso,
              impacto : el.impacto,
              ent_evi : el.ent_evi,
              estatus : el.estatus
            };
            // Agregamos la actividad al arreglo
            jActividad.actividades.push(jAct);
          }
          else{
            // Revisemos si el prefijo actual es igual
            if(prefActual == el.nombreProyecto)
            {
              // Armamos JSON de Actividad y lo agregamos
              var jActA = {
                actividad : el.actividad,
                fechacompromiso : el.fechacompromiso,
                impacto : el.impacto,
                ent_evi : el.ent_evi,
                estatus : el.estatus
              };
              // Agregamos la actividad al arreglo
              jActividad.actividades.push(jActA);
            }
            else
            {
              // Agregamos las actividades al JSON final
              jFinal.proyectos.push(jActividad);
              // Cambiamos de prefijo actual
              prefActual = el.nombreProyecto;
              // Armamos la nueva Actividad
              var jActividadN = {
                nombreProyecto : prefActual + ' - ' + name,
                email : email,
                actividades : []
              };

              var jActNuevo = {
                actividad : el.actividad,
                fechacompromiso : el.fechacompromiso,
                impacto : el.impacto,
                ent_evi : el.ent_evi,
                estatus : el.estatus
              };
              // Agregamos la actividad nueva
              jActividadN.actividades.push(jActNuevo);
              // Igualamos la actividad para vaciarla y no tener datos extra
              jActividad = jActividadN;
            }
          }
          // Armando Actividad
          if(posi == tam-1)
          {
            // Agregamos las actividades al JSON final
            jFinal.proyectos.push(jActividad);

            jCAL.message = jFinal;
            console.log('Final?');

            callback(jCAL);
          }
        });
        posi++;
      }); //forEach de actividades
     }
     else
     {
       console.log('solo una Actividad');
       shP(proyectos, act[0].nombreProyecto, function(name){
          jActividad.nombreProyecto = act[0].nombreProyecto + ' - ' + name;

          var jAct = {
            actividad : act[0].actividad,
            fechacompromiso : act[0].fechacompromiso,
            impacto : act[0].impacto,
            ent_evi : act[0].ent_evi,
            estatus : act[0].estatus
          };
          // Agregamos la actividad al arreglo
          jActividad.actividades.push(jAct);

           // Ingresando al JSON final
          jFinal.proyectos.push(jActividad);

          var jResultado = {
            status : 200,
            error : 0,
            message : jFinal
          };
          callback(jResultado);
        }); // Busca nombre proyecto
     }
    }
    else
    {
      var jResultado = {
        status : 400,
        error : 3,
        message : 'Problema en consulta de proyectos'
      };
      callback(jResultado);
    }
  });//PackageProyecto
}


/*************************************************************************************
 * Descripción: Revisa en el arreglo de proyectos y consulta por prefijo para extraer el nombre
 * Parámetro de entrada:
  @param arrProyectos: Array con los Proyectos existentes
  @param prefijo: Prefijo del proyecto a buscar el nombre
 * Parámetro de salida:
  @return callback con arreglo de JSON con los datos ordenados
 *************************************************************************************/
function shP(arrProyectos, prefijo, callback)
{
  arrProyectos.forEach(function(el){
    if(el.prefix == prefijo)
      callback(el.nombreProyecto);
  });
}

/*************************************************************************************
 * Descripción: Inserta un arreglo de usuarios a la colección correspondiente
 * Parámetro de entrada:
  @param proyectos:
 * dataUsuarios: Arreglo que contiene todos los datos de cada uno de los usuarios a guardar
 * Parámetro de salida:
  @return callback con arreglo de JSON con el resultado de la inserción
 *************************************************************************************/
 function addUsers(dataUsuarios, callback){
    var jSonResp = {
      status : 200,
      error : 0,
      message : 'Se inserto el registro correctamente',
      data : ''
    };
    if(global.conectDB)
    {
      console.log('Total de usuarios a ingresar: ' + dataUsuarios.length);
      var tam = dataUsuarios.length;
      var cuenta = 0;
      if(tam > 0)
      {
        dataUsuarios.forEach(function(usuario){
          var newReg = new PackageUsuario(usuario);
          newReg.save(function(error){
            cuenta++;
            if(error)
            {
              console.log('Error al insertar un usuario: ' + usuario.codigoEmpleado);
            }
            else
            {
              // Verificamos que se haya recorrido todos
              if(cuenta == tam-1)
              {
                console.log('Ya terminamos');
                jSonResp.message = 'Se insertaron ' + cuenta;
                callback(jSonResp);
              }
            }
          });
        });
      }
    }
    else
    {
      jSonResp.status = 404;
      jSonResp.error = 1;
      jSonResp.message = 'No se tiene conexión con MongoDB';
      callback(jSonResp);
    }
  }


/*************************************************************************************
* Descripción: Función dinámica que realiza la inseción de un nuevo documento en la coleccion que se especifica
* Parámetros de entrada:
  @param jSonDatos: JSON con los datos que se insertarán en la colección, vacios y número de folio y servicio
  @param PaqueteInserta: Hace referencia a la colección que se hará la inserción
* Parámetro de salida:
@return callback con el resultado de la operación en formato JSON
*************************************************************************************/
function guardandoRegistro(jSonDatos, PaqueteInserta, invoca, callback)
{
  var newReg = new PaqueteInserta(jSonDatos);
  var jSonResp = {
    status : 200,
    error : 0,
    message : 'Se inserto el registro correctamente',
    data : ''
  };
  if(global.conectDB)
  {
    newReg.save(function(error){
      if(error)
      {
        jSonResp.status = 400;
        jSonResp.error = 1
        jSonResp.message = error;
        callback(jSonResp);
      }
      else
      {
        var sentencia = {};
        var paquete = '';
        if(invoca == 'proyecto')
        {
          sentencia = {'nombreProyecto':jSonDatos.nombreProyecto};
          paquete = PackageProyecto;
        }
        else if(invoca == 'tarea')
        {
          sentencia = {'idproyecto':jSonDatos.idproyecto, 'actividad':jSonDatos.actividad};
          paquete = PackageTarea;
        }
        else if(invoca == 'actividad')
        {
          sentencia = {'idtarea':jSonDatos.idtarea, 'actividad':jSonDatos.actividad};
          paquete = PackageActividad;
        }
        else if(invoca == 'usuario')
        {
          sentencia = {'codigoEmpleado':jSonDatos.codigoEmpleado};
          paquete = PackageUsuario;
        }
        // Buscamos el proyecto jejejeje
        //buscaDato({'nombreProyecto': jSonDatos.nombreProyecto}, PackageProyecto).then(function(resp){
        buscaDato(sentencia, paquete).then(function(resp){
            // console.log(resp.message[0]._id);
            jSonResp.data = resp.message[0]._id;
            callback(jSonResp);
        }, function(error){
          jSonResp.status = 404;
          jSonResp.error = 1
          jSonResp.message = 'No se insertó el registro';
            callback(jSonResp);
        });
      }
    });
  }
  else {
    jSonResp.status = 404;
    jSonResp.error = 1;
    jSonResp.message = 'No se tiene conexión con MongoDB';
    callback(jSonResp);
  }
}

/*************************************************************************************
* Descripción: Busca un dato en la colección especificada
* Parámetros de entrada:
  @param SentenciaWhere: sentencia de búsqueda para la colección específica
  @param PackageName: Package correspondiente a la colección que se utilizará
* Parámetro de salida:
  @return Promise resultado de la operación en JSON
*************************************************************************************/
function buscaDato(SentenciaWhere, PackageName)
{
  var jResp = {
    status : 200,
    error : 0,
    message : ''
  };

  var condicion = (PackageName.tipo !== undefined) ? {'prefix':1}:{'_id':1};

  return new Promise(function(resolve, reject){
    PackageName.find(SentenciaWhere, function(error, resp){
      if(error)
      {
        jResp.status = 401;
        jResp.error = 1;
        jResp.message = 'Error al obtener listado de MongoDB';
        reject(jResp);
      }
      if(resp.length > 0)
      {
        jResp.message = resp;
        resolve(jResp);
      }
      else {
        var vacio = [];
        jResp.message = vacio;
        resolve(jResp);
      }
    }).sort(condicion);
  });
}

/*************************************************************************************
 * Descripción: Obtiene array con todos los proyectos existentes
 * Parámetro de entrada: Ninguno
 * Parámetro de salida:
  @return callback Array de datos
 *************************************************************************************/
function listaProyectos(callback)
{
  var jResp = {
    status : 200,
    error : 0,
    message : []
  };

  if(global.conectDB)
  {
    PackageProyecto.find({}, function(err, resp){
      // console.log(resp);
      if(err){
          jResp.status = 400;
          jResp.error = 1;
          jResp.message = err;
          callback(jResp);
      }
      else {
        jResp.message = resp;
        callback(jResp);
      }
    });
  }
  else {
    jResp.status = 400;
    jResp.error = 1;
    jResp.message = 'Error en conexión a Base de datos';
    callback(jResp);
  }
}

/*************************************************************************************
 * Descripción: Realiza la interpretación de la información enviada por Google Drive
 * Parámetro de entrada:
  @param jDatos: JSON donde concatena proyecto, tarea, actividades de los proyectos
 * Parámetro de salida:
  @return callback JSON con información de la operación de salida
 *************************************************************************************/
function manageProy(jDatos, callback)
{
  // console.log('Proyectos ' + jDatos.length);

  var jResp = {
    status : 200,
    error : 0,
    message : ' Todo OK'
  };

  // Verificamos el tamaño de los proyectos
  // console.log(jDatos);
  
  jDatos =  JSON.parse(jDatos);
  var tamProy = jDatos.projects.length;
  console.log('Tamaño: ' + tamProy);
  var indexP = 0;

  jDatos.projects.forEach(function(e){
  //async.forEach(jDatos.projects, function(e, callback){
    var jProyecto = {
      nombreProyecto : e.nombreProyecto,
      estado : e.estado,
      prefix : e.prefix
    };

     guardandoRegistro(jProyecto, PackageProyecto, 'proyecto', function(resp){
       indexP++;
       if(resp.status == 200)
       {
         console.log('Proyecto: ' + e.nombreProyecto);
         // Agregar Tareas
         //console.log(e.tareas);
         if(e.tareas !== '[]')
         {
           console.log('Tenemos tareas');
           addTask(e.tareas, e.prefix, function(res){
            // console.log('P');
           });
         }
         else {
           //console.log('no tenemos tareas');
         }
       }
       else
       {
          console.log('Falló el insertar Proyecto');
          jResp.error = 1;
          callback(jResp);
       }
       if(indexP == tamProy)
       {
         callback(jResp);
       }
     });  // Guardando Proyecto
  }); //forEach Proyectos

}

/*************************************************************************************
 * Descripción: Recorre el arreglo de tareas y las agrega a la colección correspondiente
 * efecto en cadena
 * Parámetro de entrada:
  @param arrTareas: Array con datos a insertar en colección de Tareas
  @param prefijo: Prefijo identificador del proyecto
 * Parámetro de salida:
  @return callback JSON con información de la operación
 *************************************************************************************/
function addTask(arrTareas, prefijo, callback)
{
  var jResp = {
    status : 200,
    error : 0,
    message : 'OK'
  };

  var tamTareas = arrTareas.length;
  var indexT = 0;

  // arrTareas.forEach(function(e){
  async.forEach(arrTareas, function(e, callback2){

    var jTarea = {
        idproyecto : prefijo,
        actividad : e.actividad,
        inicio : e.inicio,
        fin : e.fin,
        valor : e.valor,
        avance : e.avance,
        completado : e.completado,
        estatus : e.estatus
      };

      guardandoRegistro(jTarea, PackageTarea, 'tarea', function(resp){
        console.log('I: ' + e.actividad);
        indexT++;
        if(resp.status == 200)
        {
          if(e.actividades !== '[]')
          {
              var tamAct = e.actividades.length;
              addAct(e.actividades, e.actividad, prefijo, function(res){
                console.log(res);
              });
          }
          else {
            //console.log('No tenemos actividades');
          }
        }
        else {
          console.log('Falló el insertar Tarea');
          jResp.error = 1;
          //callback(jResp);
        }
        if(indexT == tamTareas)
        {
          //console.log('Son Iguales Tareas');
          callback2(jResp);
        }
      }); // Guardando Tarea
  });//forEach Tareas

/*
  var tamTareas = arrTareas.length;
  var indexT = 0;

  arrTareas.forEach(function(e){
    var jTarea = {
        idproyecto : prefijo,
        actividad : e.actividad,
        inicio : e.inicio,
        fin : e.fin,
        valor : e.valor,
        avance : e.avance,
        completado : e.completado,
        estatus : e.estatus
      };

      guardandoRegistro(jTarea, PackageTarea, 'tarea', function(resp){
        console.log('I: ' + e.actividad);
        indexT++;
        if(resp.status == 200)
        {
          if(e.actividades !== '[]')
          {
              var tamAct = e.actividades.length;
              addAct(e.actividades, e.actividad, prefijo, function(res){
                //console.log('``');
              });
          }
          else {
            //console.log('No tenemos actividades');
          }
        }
        else {
          console.log('Falló el insertar Tarea');
          jResp.error = 1;
          callback(jResp);
        }
        if(indexT == tamTareas)
        {
          //console.log('Son Iguales Tareas');
          callback(jResp);
        }
      }); // Guardando Tarea
  });//forEach Tareas
*/
}

/*************************************************************************************
 * Descripción: Recorre el arreglo de Actividades y las agrega a la colección correspondiente
 * Parámetro de entrada:
  @param arrAct: Array con datos a insertar en colección de Actividades
  @param nomTarea: Nombre de la tarea padre de las Actividades
  @param prefijo: Prefijo identificador del proyecto
 * Parámetro de salida:
  @return callback JSON con información de la operación
 *************************************************************************************/
function addAct(arrAct, nomTarea, prefijo, callback){
  var jResp = {
    status : 200,
    error : 0,
    message : 'OK'
  };

  var tamAct = arrAct.length;
  var indexA = 0;
  console.log('--- ----    -----    -----');
  //console.log(JSON.stringify(arrAct));
  //callback(jResp);
  async.forEach(arrAct, function(e, callback2){
    //console.log(indexA + ' - ' + nomTarea + ': ' + e.actividad);
    var jAct = {
      idtarea : nomTarea,
      nombreProyecto : prefijo,
      actividad : e.actividad,
      impacto : e.impacto,
      fechacompromiso : e.fechacompromiso,
      fechacierre : e.fechacierre,
      estatus : e.estatus,
      responsable : e.responsable,
      codigo_responsable : e.codigo_responsable,
      ent_evi : e.ent_evi
    };
    guardandoRegistro(jAct, PackageActividad, 'actividad', function(resp){
      indexA++;
      if(resp.status == 200)
      {
        console.log(indexA + '.- ' + nomTarea + ': ' + e.actividad);
      }
      else {
        console.log('Falló el insertar Actividad');
        jResp.error = 1;
      }
    }); // Guardando Actividad

    //indexA++;
    if(indexA == tamAct)
    {
      callback2(jResp);
    }
  });   //async

/*
  var tamAct = arrAct.length;
  var indexA = 0;

  arrAct.forEach(function(e){
    var jAct = {
      idtarea : nomTarea,
      nombreProyecto : prefijo,
      actividad : e.actividad,
      impacto : e.impacto,
      fechacompromiso : e.fechacompromiso,
      fechacierre : e.fechacierre,
      estatus : e.estatus,
      responsable : e.responsable,
      codigo_responsable : e.codigo_responsable,
      ent_evi : e.ent_evi
    };

    guardandoRegistro(jAct, PackageActividad, 'actividad', function(resp){
      indexA++;
      if(resp.status == 200)
      {
        console.log('. ' + nomTarea + ' - ' + e.actividad);
      }
      else {
        console.log('Falló el insertar Actividad');
        jResp.error = 1;
        callback(jResp);
      }
      if(indexA == tamAct)
      {
        //  console.log('Son Iguales Actividades');
        callback(jResp);
      }
    }); // Guardando Actividad
  });   //forEach Actividad
  */
}

/*************************************************************************************
* Descripción: Función dinámica que realiza la inseción de un nuevo documento en la
* coleccion que se especifica
* Parámetros de entrada:
  @param jSonDatos: JSON con los datos que se insertarán en la colección, vacios y número de folio y servicio
  @param PaqueteInserta: Hace referencia a la colección que se hará la inserción
* Parámetro de salida:
  @return Promise resultado de la operación
*************************************************************************************/
function insertaReg(jSonDatos, PaqueteInserta, invoca)
{
  return new Promise(function(resolve, reject){
    guardandoRegistro(jSonDatos, PaqueteInserta, invoca, function(resp){
      if(resp.status == 200)
      {
        resolve(resp);
      }
      else {
        reject(resp);
      }
    });
  });
}

/*******************************************************************************
* Descripción: Revisa las colecciones existentes, en caso de no existir
* se crean con valores base
* Parámetros de entrada: Ninguno
* Parámetro de salida:
  Callback con el resultado de la reviisón
*******************************************************************************/
function revisaBaseDatos(callback)
  {
    var jSONProyecto = {
      nombreProyecto : '',
      estado : '',
      prefix : ''
    };

    var jSONTarea = {
      idproyecto : '0',
      actividad : '',
      inicio : '',
      fin : '',
      valor : '',
      avance : '',
      completado : '',
      estatus : ''
    };

    var jSONActividad = {
      idtarea : '',
      nombreProyecto : '',
      actividad : '',
      impacto : '',
      fechacompromiso : '',
      fechacierre : '',
      estatus : '',
      responsable : '',
      codigo_responsable : '',
      ent_evi : ''
    };

    var jSONUsuario = {
      codigoEmpleado : '-1',
      email : '',
      prefijo : ['Uno', 'Dos', 'Tres']
    };


    if(global.conectDB)
    {
        // console.log("Vamos a revisar las colecciones....");
        Promise.all([
          existeColeccion(coleccionesManager.managerProyecto, PackageProyecto, jSONProyecto),
          existeColeccion(coleccionesManager.managerTarea, PackageTarea, jSONTarea),
          existeColeccion(coleccionesManager.managerActividad, PackageActividad, jSONActividad),
          existeColeccion(coleccionesManager.managerUsuario, PackageUsuario, jSONUsuario)
        ]).then(console.log("Si regresó....."));
        callback('OK');
        // Falta Manejo de Errores
    }
  }


/*******************************************************************************
* Descripción: Se encarga de revisar la existencia de una colección en caso de no existir la crear
* Parámetros de entrada:
  @param Coleccion: Manejador de la colección que se va a revisar
  @param Package: Contiene el modelo del Schema de la colección
  @param Jsondatos: Datos que se ingresarán a la colección en caso de no existir
* Parámetro de salida:
  @return Promise dependiendo el resultado
*******************************************************************************/
function existeColeccion(Coleccion, Package, JsonDatos)
{
  return new Promise(function(resolve, reject){
    revisaColecciones(Coleccion, Package, JsonDatos, function(err, content){
      if(err)
      {
        return reject(err);
      }
      resolve(content);
    });
  });
}

/*******************************************************************************
* Descripción: Revisa las colecciones para ver si tienen datos o las iniciamos
* con datos vacíos
* Parámetros de entrada:
  @param managerColeccion: Nombre del manejador de la coleccion
  @param packageRevisar: Package correspondiente para la coleccion a revisarlos
  @param jSONBase: Json con los campos base que se utilizarán para crear la coleccion
* Parámetro de salida: Ninguno
*******************************************************************************/
function revisaColecciones(managerColeccion, packageRevisar, jSONBase)
{
  // Iniciamos verificando la colección principal
  verificaColeccion(packageRevisar, function(resultado){
    if(resultado.status == 0)
    {
      // Creamos colección con datos vacíos
      creandoColeccion(managerColeccion, packageRevisar, jSONBase, function(respuesta){
        return 0;
      });
    }
    else if(resultado.status > 0)
    {
      return 0;
    }
  });
}

/*******************************************************************************
* Descripción: Permite verificar si una colección existe o contiene datos con la
* finalidad de inicializar la colección en Mongo
* Parámetros de entrada:
  @param nomColeccion: Nombre de la coleccion a verificar en base al modelo
* Parámetro de salida:
  callback con la respuesta de la verificación dela colección
*******************************************************************************/
function verificaColeccion(nomColeccion, callback)
{
  var jSonResp = {
    status : -1,
    datos : 'No existe la colección'
  };

  nomColeccion.find(function(error, respuesta){
    if(error)
    {
      //console.log("~ Hubo un error en la colección " + error);
          callback(jSonResp);
    }
    else
    {
      if(respuesta.length > 0)
      {
        jSonResp.status = 1;
        jSonResp.datos = 'OK';
      }
      else
      {
        jSonResp.status = 0;
        jSonResp.datos = 'OK';
      }
        callback(jSonResp);
      }
  });
}

/*******************************************************************************
* Descripción : Creando una colección de forma dinámica
* Parámetros de entrada:
  @param managerColeccion: Nombre del manejador de la coleccion
  @param PackageName: Package correspondiente para la coleccion a revisarlos
  @param jSONDatosGuadar: Json con los campos base que se utilizarán para crear la coleccion
* Parámetro de salida : Callback con información de la creación de la colección
*******************************************************************************/
function creandoColeccion(managerColeccion, PackageName, jSONDatosGuadar, callback)
{
  var resRetorno = {
    status : 200,
    descripcion : 'Valor por default'
  };

  managerColeccion = new PackageName(jSONDatosGuadar);
  managerColeccion.save(function(error){
    if(error)
    {
      //console.log("Función Dinámica de creación de colecciones falló: ");
      console.log(error);

      resRetorno.status = 400;
      resRetorno.descripcion = 'Hubo un problema al crear la colección';
      callback(resRetorno);

    }
    else
    {
      // Se agregan datos a Mongo
      resRetorno.descripcion = 'Se creó la colección con datos vacios';
      callback(resRetorno);
    }
  });
}
